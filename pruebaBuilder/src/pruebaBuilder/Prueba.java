package pruebaBuilder;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Prueba {

	private JFrame frame;
	private JTextField notaTp1;
	private JTextField notaTp2;
	private JTextField notaTp3;
	private JTextField notaParcial;

	
	//modificado
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Prueba window = new Prueba();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Prueba() {
		
		try //se ve como otra ventana de windows
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			
		}catch(Exception e) {
			System.out.println("Error setting native LAF:" +e);
		}
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		frame.setTitle("Calcular notas"); //titulo en ventana
		
		JLabel lblNotaTp = new JLabel("Nota tp1");
		lblNotaTp.setBounds(29, 18, 66, 34);
		frame.getContentPane().add(lblNotaTp);
		
		JLabel label = new JLabel("Nota tp2");
		label.setBounds(155, 18, 66, 34);
		frame.getContentPane().add(label);
		
		JLabel lblNotaTp_1 = new JLabel("Nota tp3");
		lblNotaTp_1.setBounds(264, 18, 66, 34);
		frame.getContentPane().add(lblNotaTp_1);
		
		JLabel lblNotaParcial = new JLabel("Nota parcial 1");
		lblNotaParcial.setBounds(405, 18, 82, 34);
		frame.getContentPane().add(lblNotaParcial);
		
		notaTp1 = new JTextField();
		notaTp1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c= e.getKeyChar();
				
				if(c<'0'||c>'9')//valido numeros, si quiero validar solo caracteres: char c= e.getKeyChar(); if((c<'a'||'c>'z')&&(c<'A'||c>'Z')) e.consume;			
					e.consume();
						
				if(notaTp1.getText().length()>=2) 
					e.consume();
				
//				if(Integer.parseInt(notaTp1.getText())<0 && Integer.parseInt(notaTp1.getText())>10)
//					e.consume();
				
			}
		});
		notaTp1.setBounds(20, 51, 86, 20);
		frame.getContentPane().add(notaTp1);
		notaTp1.setColumns(10);
		
		notaTp2 = new JTextField();
		notaTp2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c= e.getKeyChar();
				
				if(c<'0'||c>'9')
					e.consume();
				
				if(notaTp2.getText().length()>=2) 
					e.consume();
			}
		});
		notaTp2.setBounds(143, 51, 86, 20);
		frame.getContentPane().add(notaTp2);
		notaTp2.setColumns(10);
		
		notaTp3 = new JTextField();
		notaTp3.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c= e.getKeyChar();
				
				if(c<'0'||c>'9')
					e.consume();
				
				if(notaTp3.getText().length()>=2) 
					e.consume();
			}
		});
		notaTp3.setBounds(254, 51, 86, 20);
		frame.getContentPane().add(notaTp3);
		notaTp3.setColumns(10);
		
		notaParcial = new JTextField();
		notaParcial.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c= e.getKeyChar();
				
				if(c<'0'||c>'9')
					e.consume();
				
				if(notaParcial.getText().length()>=2) 
					e.consume();
			}
		});
		notaParcial.setBounds(401, 51, 86, 20);
		frame.getContentPane().add(notaParcial);
		notaParcial.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JOptionPane.showMessageDialog(null, estado());

			}
		});
		btnCalcular.setBounds(209, 122, 89, 23);
		frame.getContentPane().add(btnCalcular);
	}

	protected String estado() {
		Double tp1,tp2,tp3,parcial;
		tp1=Double.parseDouble(notaTp1.getText());
		tp2=Double.parseDouble(notaTp2.getText());
		tp3=Double.parseDouble(notaTp3.getText());
		parcial=Double.parseDouble(notaParcial.getText());
		
		if(tp1<4 || tp2<4 || tp3<4 || parcial<4)
			return "Recursa";
		
		double promedioTPs=(tp1+tp2+tp3)/3;
		double promedio=(promedioTPs+parcial)/2;
		
		return promedio>=7 ? "Promociona":"Regulariza";
		
	}
}
